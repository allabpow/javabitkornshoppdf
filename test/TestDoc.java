package test;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

public class TestDoc {

    private static final org.apache.log4j.Logger LOG4J = org.apache.log4j.Logger.getRootLogger();

    public void createPdf() {
        Document document = new Document(PageSize.A4);
        try {
            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream("/tmp/test.pdf"));
            writer.setPageEvent(new TestHeader());
            document.open();
            PdfContentByte canvas = writer.getDirectContent();
            ColumnText testColumn = new ColumnText(canvas);
            Phrase testPhrase = new Phrase();
            testPhrase.setFont(new Font(Font.FontFamily.HELVETICA, 18, Font.NORMAL, BaseColor.DARK_GRAY));
            testPhrase.add("testTextDoc"); // only after setFont(), setFont() has effect!!!
            testColumn.setSimpleColumn(testPhrase,
                    100, PageSize.A4.getHeight() - 200,
                    200, PageSize.A4.getHeight() - 160,
                    18, Element.ALIGN_LEFT);
            testColumn.go();

            document.close();
        } catch (DocumentException | FileNotFoundException ex) {
            LOG4J.error(ex.getMessage());
        }
    }
}
