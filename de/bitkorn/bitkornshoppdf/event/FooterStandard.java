package de.bitkorn.bitkornshoppdf.event;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.LineSeparator;
import de.bitkorn.bitkornshoppdf.BitkornShopPdf;
import de.bitkorn.bitkornshoppdf.db.GetConfig;

/**
 *
 * @author allapow
 */
public class FooterStandard extends PdfPageEventHelper {

    private static final org.apache.log4j.Logger LOG4J = org.apache.log4j.Logger.getRootLogger();

    @Override
    public void onStartPage(PdfWriter writer, Document document) {
        PdfContentByte canvas = writer.getDirectContent();
        
        LineSeparator hr = new LineSeparator(1, 100, null, Element.ALIGN_CENTER, -2);
        hr.drawLine(canvas,
                BitkornShopPdf.MARGIN_LEFT, PageSize.A4.getWidth() - BitkornShopPdf.MARGIN_RIGHT,
                BitkornShopPdf.MARGIN_BOTTOM + BitkornShopPdf.FOOTER_HEIGHT);

        ColumnText columnTextFooterBankData = new ColumnText(canvas);
        Phrase phraseFooterBankData = new Phrase(GetConfig.getAppConfigValue("pdf_footer_bank_data_newlines"), BitkornShopPdf.DARK_GREY_S);
        columnTextFooterBankData.setSimpleColumn(phraseFooterBankData,
                BitkornShopPdf.MARGIN_LEFT,
                BitkornShopPdf.MARGIN_BOTTOM,
                BitkornShopPdf.MARGIN_LEFT + BitkornShopPdf.FOOTER_WIDTH,
                BitkornShopPdf.MARGIN_BOTTOM + BitkornShopPdf.FOOTER_HEIGHT - 5,
                BitkornShopPdf.FONT_SIZE_M, Element.ALIGN_LEFT);
        try {
//            document.add(new Chunk(hr));
            columnTextFooterBankData.go();
        } catch (DocumentException ex) {
            LOG4J.error(ex.getMessage());
        }
    }
}
