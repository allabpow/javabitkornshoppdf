package de.bitkorn.bitkornshoppdf.event;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;
import de.bitkorn.bitkornshoppdf.BitkornShopPdf;
import static de.bitkorn.bitkornshoppdf.BitkornShopPdf.FONT_SIZE_M;
import de.bitkorn.bitkornshoppdf.db.GetConfig;
import java.io.IOException;

/**
 *
 * @author allapow
 */
public class HeaderStandard extends PdfPageEventHelper {

    private static final org.apache.log4j.Logger LOG4J = org.apache.log4j.Logger.getRootLogger();

    @Override
    public void onStartPage(PdfWriter writer, Document document) {
        PdfContentByte canvas = writer.getDirectContent();

        try {
            Image img = Image.getInstance(BitkornShopPdf.LOGO_FQN);
            img.setAbsolutePosition(
                    BitkornShopPdf.MARGIN_LEFT,
                    PageSize.A4.getHeight() - BitkornShopPdf.HEADER_MARGIN_TOP - BitkornShopPdf.LOGO_HEIGTH);
            img.scaleAbsolute(BitkornShopPdf.LOGO_WIDTH, BitkornShopPdf.LOGO_HEIGTH);
            canvas.addImage(img);
        } catch (BadElementException | IOException ex) {
            LOG4J.error("SQLException: " + ex.getMessage());
        } catch (DocumentException ex) {
            LOG4J.error("SQLException: " + ex.getMessage());
        }

        /**
         * Address Top Right
         */
        ColumnText columnTextHeaderRightAddress = new ColumnText(canvas);
        Phrase phraseHeaderRightAdress = new Phrase();
        phraseHeaderRightAdress.setFont(BitkornShopPdf.DARK_GREY_M);
        phraseHeaderRightAdress.add(GetConfig.getAppConfigValue("pdf_header_top_right_address"));
        columnTextHeaderRightAddress.setSimpleColumn(phraseHeaderRightAdress,
                PageSize.A4.getWidth() - BitkornShopPdf.MARGIN_RIGHT - BitkornShopPdf.HEADER_TOP_RIGHT_ADDRESS_WIDTH,
                PageSize.A4.getHeight() - BitkornShopPdf.HEADER_MARGIN_TOP,
                PageSize.A4.getWidth() - BitkornShopPdf.MARGIN_RIGHT,
                PageSize.A4.getHeight() - BitkornShopPdf.HEADER_MARGIN_TOP - BitkornShopPdf.HEADER_TOP_RIGHT_ADDRESS_HEIGHT,
                BitkornShopPdf.FONT_SIZE_L, Element.ALIGN_RIGHT);

        /**
         * Address Letter Window
         */
        ColumnText columnTextHeaderLetterWindowAddress = new ColumnText(canvas);
        Phrase phraseHeaderLetterWindowAddress = new Phrase();
        phraseHeaderLetterWindowAddress.setFont(BitkornShopPdf.DARK_GREY_XS_UNDERLINE);
        phraseHeaderLetterWindowAddress.add(GetConfig.getAppConfigValue("shop_address_line"));
        columnTextHeaderLetterWindowAddress.setSimpleColumn(phraseHeaderLetterWindowAddress,
                BitkornShopPdf.LETTER_WINDOW_LEFT, PageSize.A4.getHeight() - BitkornShopPdf.mmToUu(40),
                BitkornShopPdf.LETTER_WINDOW_WIDTH, PageSize.A4.getHeight() - BitkornShopPdf.mmToUu(36),
                BitkornShopPdf.FONT_SIZE_XS, Element.ALIGN_LEFT);

        try {
            columnTextHeaderRightAddress.go();
            columnTextHeaderLetterWindowAddress.go();
        } catch (DocumentException ex) {
            LOG4J.error(ex.getMessage());
        }
    }

}
