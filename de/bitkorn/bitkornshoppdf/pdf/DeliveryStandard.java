package de.bitkorn.bitkornshoppdf.pdf;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.TabSettings;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfImportedPage;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.LineSeparator;
import de.bitkorn.bitkornshoppdf.BitkornShopPdf;
import de.bitkorn.bitkornshoppdf.db.GetConfig;
import de.bitkorn.bitkornshoppdf.db.IsoCountry;
import de.bitkorn.bitkornshoppdf.event.FooterStandard;
import de.bitkorn.bitkornshoppdf.event.HeaderStandard;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Set;
import java.util.logging.Level;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.log4j.Logger;

/**
 *
 * @author allapow
 */
public class DeliveryStandard {

    private Connection conn = null;
    private static final Logger LOG4J = Logger.getRootLogger();

    private final String querySelectBasket = "SELECT sbe.*,"
            + " sdd.shop_document_delivery_number, sdd.shop_document_delivery_time,"
            + " sdo.shop_document_order_number, sdo.shop_document_order_time,"
            + " sudn.shop_user_data_number_varchar,"
            + " sdi.shop_document_invoice_time"
            + " FROM shop_basket_entity sbe"
            + " LEFT JOIN shop_document_delivery sdd USING(shop_basket_unique)"
            + " LEFT JOIN shop_document_order sdo USING(shop_basket_unique)"
            + " LEFT JOIN shop_user_data_number sudn USING(user_id)"
            + " LEFT JOIN shop_document_invoice sdi USING(shop_basket_unique)"
            + " WHERE shop_basket_unique = ?";

    /**
     *
     * @param conn
     */
    public DeliveryStandard(Connection conn) {
        this.conn = conn;
    }

    /**
     *
     * @param fqfn
     * @param shopBasketUnique
     * @return
     */
    public int createPdf(String fqfn, String shopBasketUnique) {
        try {
            PreparedStatement stmtQueryBasket = this.conn.prepareStatement(querySelectBasket);
            stmtQueryBasket.setString(1, shopBasketUnique);
            ResultSet resultBasket = stmtQueryBasket.executeQuery();
            if (resultBasket.first()) {
                Document document = new Document(PageSize.A4, BitkornShopPdf.MARGIN_LEFT, BitkornShopPdf.MARGIN_RIGHT, BitkornShopPdf.MARGIN_TOP, BitkornShopPdf.MARGIN_BOTTOM); // Margin gilt dem Content, nicht header und footer!
                PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(fqfn));
//                HeaderStandard headerStandard = new HeaderStandard();
//                writer.setPageEvent(new HeaderStandard());
//                writer.setPageEvent(new FooterStandard());
                document.open();
                document.addCreationDate();
                document.addCreator(GetConfig.getAppConfigValue("shop_owner"));
                document.addAuthor(GetConfig.getAppConfigValue("shop_owner"));
                document.addTitle(GetConfig.getAppConfigValue("shop_name"));
                document.addSubject("Delivery");
//                document.setMargins(BitkornShopPdf.MARGIN_LEFT, BitkornShopPdf.MARGIN_RIGHT, BitkornShopPdf.MARGIN_TOP, BitkornShopPdf.MARGIN_BOTTOM);
                PdfContentByte canvas = writer.getDirectContent();

                /**
                 * Letterhead
                 */
                PdfReader readerLetterhead = new PdfReader(BitkornShopPdf.LETTERHEAD_FQN);
                PdfImportedPage page01 = writer.getImportedPage(readerLetterhead, 1);
                canvas.addTemplate(page01, 0, 0);

                /**
                 * User Address
                 */
                HashMap<Integer, String> isoCountry = IsoCountry.getIsoCountryHashMap();
                Phrase phraseAddress = new Phrase();
                phraseAddress.setFont(BitkornShopPdf.DARK_GREY_M);
                String addressName = "";
                String taxId = null;
                if (resultBasket.getString("shop_user_address_invoice_id") != null && !resultBasket.getString("shop_user_address_invoice_id").isEmpty()) {
                    addressName = StringEscapeUtils.unescapeHtml4(resultBasket.getString("shop_user_address_invoice_name1") + " " + resultBasket.getString("shop_user_address_invoice_name2"));
                    phraseAddress.add(addressName);
                    phraseAddress.add(Chunk.NEWLINE);
                    phraseAddress.add(StringEscapeUtils.unescapeHtml4(resultBasket.getString("shop_user_address_invoice_street") + " " + resultBasket.getString("shop_user_address_invoice_street_no")));
                    phraseAddress.add(Chunk.NEWLINE);
                    phraseAddress.add(StringEscapeUtils.unescapeHtml4(resultBasket.getString("shop_user_address_invoice_zip") + " " + resultBasket.getString("shop_user_address_invoice_city")));
                    phraseAddress.add(Chunk.NEWLINE);
                    phraseAddress.add(isoCountry.get(resultBasket.getInt("shop_user_address_invoice_iso_country_id")));
                    phraseAddress.add(Chunk.NEWLINE);
                    taxId = resultBasket.getString("shop_user_address_tax_id");
                } else if (resultBasket.getString("shop_basket_address_invoice_id") != null && !resultBasket.getString("shop_basket_address_invoice_id").isEmpty()) {
                    addressName = StringEscapeUtils.unescapeHtml4(resultBasket.getString("shop_basket_address_invoice_name1") + " " + resultBasket.getString("shop_basket_address_invoice_name2"));
                    phraseAddress.add(addressName);
                    phraseAddress.add(Chunk.NEWLINE);
                    phraseAddress.add(StringEscapeUtils.unescapeHtml4(resultBasket.getString("shop_basket_address_invoice_street") + " " + resultBasket.getString("shop_basket_address_invoice_street_no")));
                    phraseAddress.add(Chunk.NEWLINE);
                    phraseAddress.add(StringEscapeUtils.unescapeHtml4(resultBasket.getString("shop_basket_address_invoice_zip") + " " + resultBasket.getString("shop_basket_address_invoice_city")));
                    phraseAddress.add(Chunk.NEWLINE);
                    phraseAddress.add(isoCountry.get(resultBasket.getInt("shop_basket_address_invoice_iso_country_id")));
                    phraseAddress.add(Chunk.NEWLINE);
                    taxId = resultBasket.getString("shop_basket_address_tax_id");
                } else {
                    throw new RuntimeException("basket does not have an address. BasketUnique: " + shopBasketUnique);
                }
                ColumnText textAddress = new ColumnText(canvas);
                textAddress.setSimpleColumn(phraseAddress,
                        BitkornShopPdf.LETTER_WINDOW_LEFT, BitkornShopPdf.LETTER_WINDOW_LEFT_Y,
                        BitkornShopPdf.LETTER_WINDOW_WIDTH, BitkornShopPdf.LETTER_WINDOW_RIGHT_Y,
                        BitkornShopPdf.FONT_SIZE_L, Element.ALIGN_LEFT);
                textAddress.go();

                /**
                 * Top Right
                 */
                ColumnText columnTextTopRight = new ColumnText(canvas);
                Phrase phraseTopRight = new Phrase();
                phraseTopRight.setTabSettings(new TabSettings(56f));
                phraseTopRight.setFont(BitkornShopPdf.DARK_GREY_M_BOLD);
                phraseTopRight.add("  L I E F E R S C H E I N");
                phraseTopRight.setFont(BitkornShopPdf.DARK_GREY_S);
                phraseTopRight.add(Chunk.NEWLINE);
                phraseTopRight.add("Nummer:");
                phraseTopRight.add(Chunk.TABBING);
                phraseTopRight.add(resultBasket.getString("shop_document_delivery_number"));
                phraseTopRight.add(Chunk.NEWLINE);
                if (resultBasket.getInt("user_id") > 0) {
                    phraseTopRight.add("Kunde:");
                    phraseTopRight.add(Chunk.TABBING);
                    phraseTopRight.add(resultBasket.getString("shop_user_data_number_varchar"));
                    phraseTopRight.add(Chunk.NEWLINE);
                }
                phraseTopRight.add("Datum:");
                phraseTopRight.add(Chunk.TABBING);
                Date dateInvoice = new Date(resultBasket.getLong("shop_document_invoice_time") * 1000);
//                Calendar calendarInvoice = Calendar.getInstance(Locale.GERMANY);
//                calendarInvoice.setTime(dateInvoice);
                // first month (January) is 0; therefore MONTH plus 1
//                phraseTopRight.add(BitkornShopPdf.padLeft(calendarInvoice.get(Calendar.DAY_OF_MONTH) + "", 2, '0') + "." + BitkornShopPdf.padLeft(calendarInvoice.get(Calendar.MONTH) + 1 + "", 2, '0') + "." + calendarInvoice.get(Calendar.YEAR));
                phraseTopRight.add(BitkornShopPdf.DISPLAYTIME_FORMAT.format(dateInvoice));
                phraseTopRight.add(Chunk.NEWLINE);
                phraseTopRight.setFont(BitkornShopPdf.DARK_GREY_XS);
                phraseTopRight.add("Rechnungsdatum = Lieferdatum");
                phraseTopRight.add(Chunk.NEWLINE);
                phraseTopRight.add(Chunk.NEWLINE);
                phraseTopRight.setFont(BitkornShopPdf.DARK_GREY_S);
                phraseTopRight.add("Bestellung:");
                phraseTopRight.add(Chunk.TABBING);
                phraseTopRight.add(resultBasket.getString("shop_document_order_number"));
                phraseTopRight.add(Chunk.NEWLINE);
                phraseTopRight.add("Datum:");
                phraseTopRight.add(Chunk.TABBING);
                Date dateOrder = new Date(resultBasket.getLong("shop_document_order_time") * 1000);
//                Calendar calendarOrder = Calendar.getInstance(Locale.GERMANY);
//                calendarOrder.setTime(dateOrder);
                // first month (January) is 0; therefore MONTH plus 1
//                phraseTopRight.add(BitkornShopPdf.padLeft(calendarOrder.get(Calendar.DAY_OF_MONTH) + "", 2, '0') + "." + BitkornShopPdf.padLeft(calendarOrder.get(Calendar.MONTH) + 1 + "", 2, '0') + "." + calendarOrder.get(Calendar.YEAR));
                phraseTopRight.add(BitkornShopPdf.DISPLAYTIME_FORMAT.format(dateOrder));
                phraseTopRight.add(Chunk.NEWLINE);
                if (taxId != null) {
                    phraseTopRight.add("USt.-ID:");
                    phraseTopRight.add(Chunk.TABBING);
                    phraseTopRight.add(taxId);
                    phraseTopRight.add(Chunk.NEWLINE);
                }
                phraseTopRight.add("Versand:");
                phraseTopRight.add(Chunk.TABBING);
                phraseTopRight.add(resultBasket.getString("shipping_method"));

                columnTextTopRight.setSimpleColumn(phraseTopRight,
                        PageSize.A4.getWidth() - BitkornShopPdf.MARGIN_RIGHT - BitkornShopPdf.HEADER_TOP_RIGHT_ADDRESS_WIDTH,
                        PageSize.A4.getHeight() - BitkornShopPdf.HEADER_MARGIN_TOP,
                        PageSize.A4.getWidth() - BitkornShopPdf.MARGIN_RIGHT,
                        PageSize.A4.getHeight() - BitkornShopPdf.HEADER_MARGIN_TOP - BitkornShopPdf.HEADER_TOP_RIGHT_ADDRESS_HEIGHT,
                        BitkornShopPdf.FONT_SIZE_M, Element.ALIGN_LEFT);
                columnTextTopRight.go();

                /**
                 * Ueberschrift (Lieferschein 12345)
                 */
                Phrase phraseInvoiceH1 = new Phrase();
                phraseInvoiceH1.setFont(BitkornShopPdf.DARK_GREY_L_BOLD);
                phraseInvoiceH1.add("Lieferschein " + resultBasket.getString("shop_document_delivery_number"));
                ColumnText textInvoiceH1 = new ColumnText(canvas);
                textInvoiceH1.setSimpleColumn(phraseInvoiceH1,
                        BitkornShopPdf.MARGIN_LEFT, PageSize.A4.getHeight() - BitkornShopPdf.mmToUu(95),
                        BitkornShopPdf.mmToUu(120), PageSize.A4.getHeight() - BitkornShopPdf.mmToUu(85),
                        BitkornShopPdf.FONT_SIZE_L, Element.ALIGN_LEFT);
                textInvoiceH1.go();

                /**
                 * Anrede und Einleitung
                 */
                Phrase phraseIntroduction = new Phrase();
                phraseIntroduction.setFont(BitkornShopPdf.DARK_GREY_M);
                phraseIntroduction.add("Sehr geehrte/r " + addressName + ",");
                phraseIntroduction.add(Chunk.NEWLINE);
                phraseIntroduction.add(Chunk.NEWLINE);
                phraseIntroduction.add(GetConfig.getAppConfigValue("pdf_delivery_introduction"));
                ColumnText textIntroduction = new ColumnText(canvas);
                textIntroduction.setSimpleColumn(phraseIntroduction,
                        BitkornShopPdf.MARGIN_LEFT, PageSize.A4.getHeight() - BitkornShopPdf.mmToUu(120),
                        PageSize.A4.getWidth() - BitkornShopPdf.MARGIN_RIGHT, PageSize.A4.getHeight() - BitkornShopPdf.mmToUu(100),
                        BitkornShopPdf.FONT_SIZE_M, Element.ALIGN_LEFT);
                textIntroduction.go();

                /**
                 * fuers table.setSpacingBefore()
                 * (ab hier nur noch relative Positionen)
                 */
                Paragraph paragraph = new Paragraph(" "); // MUSS wenigstens n Blank drin sein!!!
                document.add(paragraph);
                /**
                 * Tabelle mit Produktlisting
                 */
                PdfPTable table = new PdfPTable(3);
                table.setWidthPercentage(90);
                table.setHorizontalAlignment(0);
                table.setSpacingBefore(BitkornShopPdf.mmToUu(70));
                int[] colWidths = {10, 70, 20};
                table.setWidths(colWidths);
                PdfPCell cell;
                cell = new PdfPCell(new Phrase(BitkornShopPdf.FONT_SIZE_M, "Anzahl", BitkornShopPdf.DARK_GREY_M));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
                table.addCell(cell);
                cell = new PdfPCell(new Phrase(BitkornShopPdf.FONT_SIZE_M, "Artikel Bezeichnung", BitkornShopPdf.DARK_GREY_M));
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
                table.addCell(cell);
                cell = new PdfPCell(new Phrase(BitkornShopPdf.FONT_SIZE_M, "Artikel-Nr.", BitkornShopPdf.DARK_GREY_M));
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
                table.addCell(cell);
                /*
                 * Produktlisting
                 */
                ArticleOptions articleOptions = new ArticleOptions(conn);
                Set<String> optionStringsSet;
                do {
                    cell = new PdfPCell(new Phrase(BitkornShopPdf.FONT_SIZE_M, resultBasket.getString("shop_article_amount"), BitkornShopPdf.DARK_GREY_M));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    table.addCell(cell);
                    cell = new PdfPCell();
                    cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                    cell.addElement(new Phrase(BitkornShopPdf.FONT_SIZE_M, resultBasket.getString("shop_article_name"), BitkornShopPdf.DARK_GREY_M));
                    if (articleOptions.computeArticleOptions(resultBasket.getString("shop_basket_item_article_options")).isValid()) {
                        for (String optionString : articleOptions.getOptionStringsSet()) {
//                            cell.addElement(Chunk.NEWLINE);
                            cell.addElement(new Phrase(BitkornShopPdf.FONT_SIZE_S, optionString, BitkornShopPdf.DARK_GREY_S));
                        }
                    }
                    cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                    table.addCell(cell);
                    cell = new PdfPCell(new Phrase(BitkornShopPdf.FONT_SIZE_M, resultBasket.getString("shop_article_sku"), BitkornShopPdf.DARK_GREY_M));
                    cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    table.addCell(cell);
                    table.completeRow();
                } while (resultBasket.next());

                resultBasket.last();
                
                document.add(table);
                /*
                 * prevent "IOException: The document has no pages" error which often occurs when the document contains no meaningful
                 */
//                document.add(new Chunk("leerer Chunk"));
                document.close();
            }
        } catch (SQLException | DocumentException | FileNotFoundException ex) {
            LOG4J.error(ex.getLocalizedMessage());
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(DeliveryStandard.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 1;
    }

}
