
package de.bitkorn.bitkornshoppdf.pdf;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author allapow
 */
public class ArticleOptions {

    private Connection conn = null;
    private static final Logger LOG4J = Logger.getRootLogger();

    private boolean valid = false;
    private final Set<String> optionStringsSet = new HashSet();

    private final String querySelectArticleOptions = "SELECT * FROM shop_article_option_item WHERE shop_article_option_item_id=?";

    /**
     *
     * @param conn
     */
    public ArticleOptions(Connection conn) {
        this.conn = conn;
    }

    public ArticleOptions computeArticleOptions(String basketItemArticleOptions) {
        try {
            JSONArray optionsJson = new JSONArray(basketItemArticleOptions);
            optionStringsSet.clear();
            if (optionsJson.length() < 1) {
                valid = false;
            }
            PreparedStatement stmt = this.conn.prepareStatement(querySelectArticleOptions);
            int optionitemid;
            for (int i = 0; i < optionsJson.length(); i++) {
                optionitemid = optionsJson.getJSONObject(i).getInt("optionitemid");
                stmt.setInt(1, optionitemid);
                ResultSet result = stmt.executeQuery();
                if (!result.first()) {
                    LOG4J.error("BasketEntity has unvalid ArticleOptions (optionitemid): " + basketItemArticleOptions);
                    valid = false;
                }
                optionStringsSet.add(result.getString("shop_article_option_item_view_name") + ": " + result.getString("shop_article_option_item_view_value"));
            }

            valid = true;
        } catch (SQLException | JSONException ex) {
            LOG4J.error(ex.getLocalizedMessage());
        }
        return this;
    }

    public boolean isValid() {
        return valid;
    }

    public Set<String> getOptionStringsSet() {
        return optionStringsSet;
    }

}
