package de.bitkorn.bitkornshoppdf.db;

import de.bitkorn.bitkornshoppdf.BitkornShopPdf;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import org.apache.log4j.Logger;

/**
 *
 * @author allapow
 */
public class IsoCountry {

    private static final Logger LOG4J = Logger.getRootLogger();
    private static final String QUERY_ISO_COUNTRY = "SELECT * FROM iso_country";

    public static HashMap<Integer, String> getIsoCountryHashMap() {
        HashMap<Integer, String> isoCountry = new HashMap();
        try {
            Statement stmt = BitkornShopPdf.getApp().getDb().getConn().createStatement();
            ResultSet result = stmt.executeQuery(String.format(QUERY_ISO_COUNTRY));
            if (result.first()) {
                do {
                    isoCountry.put(result.getInt("iso_country_id"), result.getString("iso_country_iso") + " - " + result.getString("iso_country_name"));
                } while (result.next());
            }
        } catch (SQLException ex) {
            LOG4J.error("SQLException: " + ex.getMessage());
            LOG4J.error("SQLState: " + ex.getSQLState());
            LOG4J.error("VendorError: " + ex.getErrorCode());
        }
        return isoCountry;
    }
}
