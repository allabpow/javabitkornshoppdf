
package de.bitkorn.bitkornshoppdf.db;

import de.bitkorn.bitkornshoppdf.BitkornShopPdf;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.apache.log4j.Logger;

/**
 *
 * @author allapow
 */
public class GetConfig {
    
    private static final Logger LOG4J = Logger.getRootLogger();
    private static final String QUERY_CONFIG_VALUE = "SELECT shop_configuration_value FROM shop_configuration WHERE shop_configuration_key='%s'";
    
    public static String getAppConfigValue(String appConfigName) {
        try {
            Statement stmt = BitkornShopPdf.getApp().getDb().getConn().createStatement();
            ResultSet result = stmt.executeQuery(String.format(QUERY_CONFIG_VALUE, appConfigName));
            if (result.first()) {
                return result.getString("shop_configuration_value");
            }
        } catch (SQLException ex) {
            LOG4J.error("SQLException: " + ex.getMessage());
            LOG4J.error("SQLState: " + ex.getSQLState());
            LOG4J.error("VendorError: " + ex.getErrorCode());
        }
        return "keine db.shop_configuration.shop_configuration_value";
    }
}
