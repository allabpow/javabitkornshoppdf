
package de.bitkorn.bitkornshoppdf.db;

import de.bitkorn.bitkornshoppdf.BitkornShopPdf;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import org.apache.log4j.Logger;

/**
 *
 * @author allapow
 */
public class JdbcAccess {

    private Connection conn = null;
    private static final Logger LOG4J = Logger.getRootLogger();

    public boolean connect() {
        try {
            /*
             * conn = DriverManager.getConnection("jdbc:mysql://localhost/bitkornshop?" + "user=root&password=sqlkomuni");
             */
            conn = DriverManager.getConnection(BitkornShopPdf.SQL_DRIVER_CONNECTION_STRING);
            if (!conn.isClosed()) {
                return true;
            }
        } catch (SQLException ex) {
            LOG4J.error("SQLException: " + ex.getMessage());
            LOG4J.error("SQLState: " + ex.getSQLState());
            LOG4J.error("VendorError: " + ex.getErrorCode());
        }
        return false;
    }

    /**
     * 
     * @return The connection
     */
    public Connection getConn() {
        return conn;
    }
}
