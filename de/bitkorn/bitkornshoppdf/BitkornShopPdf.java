package de.bitkorn.bitkornshoppdf;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import de.bitkorn.bitkornshoppdf.db.JdbcAccess;
import de.bitkorn.bitkornshoppdf.pdf.DeliveryStandard;
import de.bitkorn.bitkornshoppdf.pdf.InvoiceStandard;
import de.bitkorn.bitkornshoppdf.pdf.OrderStandard;
import java.io.IOException;
import java.net.URISyntaxException;
import java.security.CodeSource;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.SimpleLayout;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import test.TestDoc;

/**
 * https://de.wikipedia.org/wiki/A4-Format Portrait: 210 x 297 mm Landscape: 297 x 210 mm http://developers.itextpdf.com/question/where-origin-xy-pdf-page
 * http://developers.itextpdf.com/question/how-get-userunit-pdf-file
 * http://developers.itextpdf.com/content/best-itext-questions-stackoverview/inspect-pdf/itext7-how-get-userunit-pdf-file In short, 1 in. = 25.4 mm = 72 user
 * units (which roughly corresponds to 72 pt).
 *
 * @author allapow
 */
public class BitkornShopPdf {

    private static BitkornShopPdf app;

    private static final Logger LOG4J = Logger.getRootLogger();
    private JdbcAccess db;

    public static final String FILE_SEPERATOR = "/"; // weil CodeSource auch unter Windows einen Slash hat
    public static final String CONFIG_DIRECTORY = getConfigDirectory(false);
    private static final String CONF_XML_FQN = CONFIG_DIRECTORY + "bitkornshop.xml";
    public static final String LOGO_FQN = CONFIG_DIRECTORY + "logo.png";
    public static final String LETTERHEAD_FQN = CONFIG_DIRECTORY + "letterhead.pdf";

    public static final String SQL_DRIVER_CONNECTION_STRING = parseXmlConfigForMysql();

    public static final SimpleDateFormat LOGTIME_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss::S");
    public static final SimpleDateFormat DISPLAYTIME_FORMAT = new SimpleDateFormat("dd.MM.yyyy");

    /**
     * some values
     */
//    public static final int WIDTH_A4_PORTRAIT = 595; // 210 mm
//    public static final int HEIGTH_A4_PORTRAIT = 842; // 297 mm
    public static final float FONT_SIZE_XS = mmToUu(2);
    public static final float FONT_SIZE_S = mmToUu(3);
    public static final float FONT_SIZE_M = mmToUu(4);
    public static final float FONT_SIZE_L = mmToUu(5);
    public static final float FONT_SIZE_XL = mmToUu(6);
    public static final float FONT_SIZE_XXL = mmToUu(7);
    public static final int HEADER_MARGIN_TOP = 60;
    public static final int HEADER_TOP_RIGHT_ADDRESS_WIDTH = 220;
    public static final int HEADER_TOP_RIGHT_ADDRESS_HEIGHT = 140;
    public static final int MARGIN_LEFT = 50;
    public static final int MARGIN_RIGHT = 30;
    public static final int MARGIN_TOP = HEADER_TOP_RIGHT_ADDRESS_HEIGHT + 10;
    public static final int MARGIN_BOTTOM = 20;
    public static final int FOOTER_WIDTH = 240;
    public static final int FOOTER_HEIGHT = 60;
    public static final float LETTER_WINDOW_LEFT = MARGIN_LEFT + mmToUu(7);
    public static final float LETTER_WINDOW_WIDTH = mmToUu(100);
    public static final float LETTER_WINDOW_LEFT_Y = PageSize.A4.getHeight() - BitkornShopPdf.mmToUu(70);
    public static final float LETTER_WINDOW_RIGHT_Y = PageSize.A4.getHeight() - BitkornShopPdf.mmToUu(50);

    // aktuell bei 72dpi: 160 x 109
    public static final float LOGO_WIDTH = 80f; // Bild muss 72 DPI haben ...bspw. beim Inkscape-Export
    public static final float LOGO_HEIGTH = 54.5f; // Bild muss 72 DPI haben ...bspw. beim Inkscape-Export
    public static final BaseColor BK_BASECOLOR = new BaseColor(122, 30, 77);
    public static final Font DARK_GREY_XS = new Font(Font.FontFamily.HELVETICA, FONT_SIZE_XS, Font.NORMAL, BaseColor.DARK_GRAY);
    public static final Font DARK_GREY_S = new Font(Font.FontFamily.HELVETICA, FONT_SIZE_S, Font.NORMAL, BaseColor.DARK_GRAY);
    public static final Font DARK_GREY_XS_UNDERLINE = new Font(Font.FontFamily.HELVETICA, FONT_SIZE_XS, Font.UNDERLINE, BaseColor.DARK_GRAY);
    public static final Font DARK_GREY_S_UNDERLINE = new Font(Font.FontFamily.HELVETICA, FONT_SIZE_S, Font.UNDERLINE, BaseColor.DARK_GRAY);
    public static final Font DARK_GREY_M = new Font(Font.FontFamily.HELVETICA, FONT_SIZE_M, Font.NORMAL, BaseColor.DARK_GRAY);
    public static final Font DARK_GREY_M_BOLD = new Font(Font.FontFamily.HELVETICA, FONT_SIZE_M, Font.BOLD, BaseColor.DARK_GRAY);
    public static final Font DARK_GREY_L = new Font(Font.FontFamily.HELVETICA, FONT_SIZE_L, Font.NORMAL, BaseColor.DARK_GRAY);
    public static final Font DARK_GREY_L_BOLD = new Font(Font.FontFamily.HELVETICA, FONT_SIZE_L, Font.BOLD, BaseColor.DARK_GRAY);
    public static final Font DARK_GREY_XL = new Font(Font.FontFamily.HELVETICA, FONT_SIZE_XL, Font.BOLD, BaseColor.DARK_GRAY);
    public static final Font DARK_GREY_XXL = new Font(Font.FontFamily.HELVETICA, FONT_SIZE_XXL, Font.BOLD, BaseColor.DARK_GRAY);
    public static final Font WARN_M = new Font(Font.FontFamily.HELVETICA, FONT_SIZE_M, Font.NORMAL, BaseColor.ORANGE);
    public static final Font CRITICAL_M = new Font(Font.FontFamily.HELVETICA, FONT_SIZE_M, Font.NORMAL, BaseColor.RED);

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        app = new BitkornShopPdf();
        app.logging();
        Date start = new Date();
        LOG4J.info("Start: " + LOGTIME_FORMAT.format(start));
        app.createDb();
        // END basic startup
        if (args.length >= 1) {
            app.runOperation(args);
        }
        // END run app
        Date end = new Date();
        LOG4J.info("End: " + LOGTIME_FORMAT.format(end));
        long diff = Math.abs(end.getTime() - start.getTime());
        Date duration = new Date(diff - 3600000);
        LOG4J.info("Duration: " + LOGTIME_FORMAT.format(duration));
    }

    /**
     * Run one operation.
     *
     * @param args
     */
    private void runOperation(String[] args) {
        if (args.length == 3) {
            String shopBasketUnique = args[0];
            String documentType = args[1];
            String fqfn = args[2];
            if (documentType.equalsIgnoreCase("invoice")) {
                InvoiceStandard invoiceStandard = new InvoiceStandard(db.getConn());
                invoiceStandard.createPdf(fqfn, shopBasketUnique);
            } else if (documentType.equalsIgnoreCase("order")) {
                OrderStandard orderStandard = new OrderStandard(db.getConn());
                orderStandard.createPdf(fqfn, shopBasketUnique);
            } else if (documentType.equalsIgnoreCase("delivery")) {
                DeliveryStandard deliveryStandard = new DeliveryStandard(db.getConn());
                deliveryStandard.createPdf(fqfn, shopBasketUnique);
            }
        } else if (args.length == 1) {
            String argOne = args[0];
            if (argOne.equalsIgnoreCase("test")) {
                TestDoc testDoc = new TestDoc();
                testDoc.createPdf();
            }
        }
    }

    /**
     *
     * @param cut
     * @return
     */
    private static String getConfigDirectory(boolean cut) {
        CodeSource cs = BitkornShopPdf.class.getProtectionDomain().getCodeSource();
        String myPath = "";
        try {
            myPath = cs.getLocation().toURI().getPath();
//            System.out.println("CodeSource: " + myPath);
        } catch (URISyntaxException ex) {
            System.err.println(ex.getMessage());
        }
        // absolute Pfad zum JAR-File plus endenden Slash
        myPath = myPath.substring(0, myPath.lastIndexOf(FILE_SEPERATOR) + 1);
        if (cut) {
            myPath = myPath.substring(0, myPath.length() - 1); // letzten Slash killen
            myPath = myPath.substring(0, myPath.lastIndexOf(FILE_SEPERATOR) + 1); // letzten Ordner (hier ja ohne den letzten Slash) killen
        }
//        System.out.println("myPath: " + myPath);
        return myPath;
    }

    /**
     *
     * @return
     */
    private static String parseXmlConfigForMysql() {
        try {
            Document doc = new SAXBuilder().build(CONF_XML_FQN);
            Element root = doc.getRootElement();
            Element sql = root.getChild("mysql");

            String dbUrl = sql.getChildText("db_url");
            String dbUser = sql.getChildText("db_user");
            String dbPasswd = sql.getChildText("db_passwd");
            return dbUrl + "?user=" + dbUser + "&password=" + dbPasswd;
        } catch (JDOMException | IOException e) {
            System.err.println(e.getMessage());
        }
        return "";
    }

    /**
     *
     */
    private void logging() {
        try {
            SimpleLayout layout = new SimpleLayout();
            FileAppender fileAppender = new FileAppender(layout, CONFIG_DIRECTORY + "bitkornshop.log", true);
            LOG4J.addAppender(fileAppender);
            // ALL | DEBUG | INFO | WARN | ERROR | FATAL | OFF:
            LOG4J.setLevel(Level.ALL);
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
        }
    }

    /**
     *
     */
    private void createDb() {
        db = new JdbcAccess();
        if (db.connect()) {
            System.out.println("db is open");
//            LOG4J.info("db is open");
        }
    }

    public static BitkornShopPdf getApp() {
        if (null == app) {
            throw new RuntimeException("There is no App Instance");
        }
        return app;
    }

    public JdbcAccess getDb() {
        return db;
    }

    /**
     * mm * 2,834645669
     *
     * @param mm
     * @return User Unit
     */
    public static float mmToUu(float mm) {
        return mm * 72 / 25.4f;
    }

    public static int mmToUuInt(int mm) {
        return mm * 3;
    }

    public static String padLeft(String s, int n, Character p) {
        return String.format("%1$" + n + "s", s).replace(' ', p);
    }
}
